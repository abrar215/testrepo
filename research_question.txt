Group: group 188

Question
========

RQ: Is there a correlation between weight and acceleration among cars?

Null hypothesis: There is no correlation between weight and acceleration among cars.

Alternative hypothesis: There is a correlation between weight and acceleration among cars.

Dataset
=======

URL: https://www.kaggle.com/uciml/autompg-dataset


Column Headings:

```
> auto_mpg <- read_csv("auto-mpg.csv")
> colnames(auto_mpg)
[1] "mpg"          "cylinders"    "displacement" "horsepower"   "weight"      
[6] "acceleration" "model year"   "origin"       "car name"

```